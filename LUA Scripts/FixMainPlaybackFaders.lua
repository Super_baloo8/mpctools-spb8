-- 	Fix Main Playback Faders
-- 		Author: Ricardo Dias <ricdays@gmail.com>
--
-- This script copies a range of main playbacks from 1 bank to other banks

-------------------
-- Configuration --
-------------------

sourceBankNumber		= 1		-- The bank number to copy from
sourcePlaybackNumStart	= 1		-- The first playback to copy
sourcePlaybackNumEnd	= 3		-- The last playback to copy

targetBanks = {2, 3, 4, 5}		-- Target banks where playbacks will be copied to


----------------------------------------------------
-- Main Script - dont change if you don't need to --
----------------------------------------------------

ClearProgrammer(); -- Make sure the programmer is cleared

for playbackNum = sourcePlaybackNumStart, sourcePlaybackNumEnd do
	cuelistNumber = GetCuelistNumberFromMainPlayback(sourceBankNumber, playbackNum)
	
	for i,targetBank in ipairs(targetBanks) do
		CopyCuelistFromDirectoryToMainPlayback(cuelistNumber, targetBank, playbackNum)
	end
end

ClearProgrammer(); -- Make sure the programmer is cleared in the end