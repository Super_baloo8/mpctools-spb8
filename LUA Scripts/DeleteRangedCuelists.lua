-- 	Delete Ranged Cuelists
--  Created By Nadav Yarkoni <Nadav@Yarkoni.co.il>
--  Donate To Ricardo Dias For Creating MPC Tools!
--
-------------------
-- Configuration --
-------------------


valueStart = tonumber(Prompt("Delete Cuelist From:", ""))

valueEnd = tonumber(Prompt("Delete Until:", ""))

YesOrNo = PromptYesNo("Are you sure to delete Culist number " .. valueStart .. " to " .. valueEnd .. "?", "")
print(YesOrNo)

print("Deleted Cuelist Number:")

if YesOrNo == true

then

----------------------------------------------------
-- Main Script - dont change if you don't need to --
----------------------------------------------------

ClearProgrammer(); -- Make sure the programmer is cleared

for CuelistNum = valueStart, valueEnd do
	
	DeleteCuelist(CuelistNum)
	print(CuelistNum)
	ClearProgrammer();
end
print("Scripted By Nadav Yarkoni <Nadav@Yarkoni.co.il>")
end
