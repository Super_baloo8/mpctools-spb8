Credits to Harley K. Morgan

Here is my device file for a Novation Launchpad S.

Buttons are labeled Left to Right, Top to Bottom by Row and Button #.

01x01 = Row 1 Button 1
05x08 = Row 5 Button 8

This template is set for Midi Channel 1 using the X-Y layout.

The Status Byte and Note is already pre-set for each button.  The user just needs to enter the velocity to set button color. 